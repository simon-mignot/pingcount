//
// Created by Simon Mignot on 04/12/17.
//

#include "PingCountAPI.h"

int PingCountAPI::m_pingCount = 0;

void PingCountAPI::setupRoutes(Pistache::Rest::Router& router)
{
    Pistache::Rest::Routes::Get(router, "/ping", Pistache::Rest::Routes::bind(&PingCountAPI::ping));
    Pistache::Rest::Routes::Get(router, "/count", Pistache::Rest::Routes::bind(&PingCountAPI::count));
}

void PingCountAPI::ping(const Pistache::Rest::Request& request, Pistache::Http::ResponseWriter response)
{
    ++m_pingCount;
    JSONObject jsonObject;
    jsonObject["message"] = new JSONValue<std::string>("pong");
    response.setMime(MIME(Application, Json));
    response.send(Pistache::Http::Code::Ok, jsonObject.toJSON());
}

void PingCountAPI::count(const Pistache::Rest::Request& request, Pistache::Http::ResponseWriter response)
{
    JSONObject jsonObject;
    jsonObject["pingCount"] = new JSONValue<int>(m_pingCount);
    response.setMime(MIME(Application, Json));
    response.send(Pistache::Http::Code::Ok, jsonObject.toJSON());
}

