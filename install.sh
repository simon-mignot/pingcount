#!/bin/bash

### Dépendances
sudo apt-get update
sudo apt-get install -y libcurl4-openssl-dev build-essential libtool cmake autoconf

### Création d'un repertoire temporaire
mkdir pingcount-install-tmp
cd pingcount-install-tmp    # BASE_DIR/pingcount-install-tmp/

#### Bibliothèques
### Pistache
## Récuperation des sources
git clone https://github.com/oktal/pistache.git
cd pistache                 # BASE_DIR/pingcount-install-tmp/pistache/
git submodule update --init

## Compilation
mkdir build
cd build                    # BASE_DIR/pingcount-install-tmp/pistache/build/
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
make

## Installation sur le système
sudo make install

cd ../..                    # BASE_DIR/pingcount-install-tmp/
### restclient-cpp
## Récupération des sources
git clone https://github.com/mrtazz/restclient-cpp.git
cd restclient-cpp           # BASE_DIR/pingcount-install-tmp/restclient-cpp/

## Configuration
./autogen.sh
./autogen.sh
./configure

## Compilation et installation
sudo make install

cd ../..                    # BASE_DIR/
### Suppression du répertoire temporaire
sudo rm -rf ./pingcount-install-tmp/

#### Mise à jour de la liste des bibliothèques
sudo ldconfig


#### Installation de PingCount
### Récuperation des sources de PingCount
mkdir -p pingcount/src
cd pingcount/src            # BASE_DIR/pingcount/src/
git clone https://bitbucket.org/simon-mignot/pingcount.git .

### Génération des fichiers de compilation
mkdir cmake-build-release
cd cmake-build-release      # BASE_DIR/pingcount/src/cmake-build-release/
cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" ../

### Compilation
make

### Installation
mv PingCount ../../PingCount

