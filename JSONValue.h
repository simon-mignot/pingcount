//
// Created by Simon Mignot on 01/12/17.
//

#ifndef PINGCOUNT_JSONVALUE_H
#define PINGCOUNT_JSONVALUE_H

#include <ostream>

#include "JSON.h"

template<class ValueType>
class JSONValue : public JSONAbstract
{
    public:
        JSONValue(ValueType v);

        JSONValue& operator=(const ValueType& v);

        template <class T>
        friend std::ostream& operator<<(std::ostream& o, JSONValue<T> const& jsonValue);

        std::string toJSON() const;
        ValueType get() const;

    private:

        ValueType m_value;
};

template <class ValueType>
JSONValue<ValueType>::JSONValue(ValueType v) : m_value(v)
{

}

template <class ValueType>
JSONValue<ValueType>& JSONValue<ValueType>::operator=(const ValueType& v)
{
    m_value = v;
    return *this;
}

template<class ValueType>
std::ostream& operator<<(std::ostream& o, JSONValue<ValueType> const& jsonValue)
{
    o << jsonValue.toJSON();
    return o;
}

template <class ValueType>
std::string JSONValue<ValueType>::toJSON() const
{
    // Maybe use stringstream for the conversion
    return std::to_string(m_value);
}

template <class ValueType>
ValueType JSONValue<ValueType>::get() const
{
    return m_value;
}

template <>
std::string JSONValue<std::string>::toJSON() const;
template <>
std::string JSONValue<bool>::toJSON() const;

#endif //PINGCOUNT_JSONVALUE_H