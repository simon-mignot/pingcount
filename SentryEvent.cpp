//
// Created by Simon Mignot on 01/12/17.
//

#include "SentryEvent.h"
#include "SentryClient.h"

SentryEvent::SentryEvent(const std::string& message)
{
    m_body["event_id"] = new JSONValue<std::string>(genUUID());
    m_body["logger"] = new JSONValue<std::string>("SentryEvent");
    m_body["platform"] = new JSONValue<std::string>("c");
    m_body["sdk"] = SentryClient::getSdkInfos();
    m_body["message"] = new JSONValue<std::string>(message);
}

std::string SentryEvent::genUUID()
{
    std::string result;
    std::string chars = "0123456789abcdef";
    for(int i = 0; i < 32; ++i)
        result += chars[std::rand() % chars.size()];

    return result;
}

void SentryEvent::setTimestamp(time_t timestamp)
{
    char buffer[50];
    strftime(buffer, 50, "%FT%T", gmtime(&timestamp));
    std::string iso8601(buffer);
    m_body["timestamp"] = new JSONValue<std::string>(iso8601);
}

std::string SentryEvent::getJSONBody()
{
    return m_body.toJSON();
}

SentryEvent& SentryEvent::setLevel(SentryEvent::Level level)
{
     std::string levelString = std::map<Level, std::string> {{Level::FATAL, "fatal"},
                                                             {Level::ERROR, "error"},
                                                             {Level::WARNING, "warning"},
                                                             {Level::INFO, "info"},
                                                             {Level::DEBUG, "debug"}}[level];
    m_body["level"] = new JSONValue<std::string>(levelString);
    return *this;
}

SentryEvent& SentryEvent::setCulprit(const std::string& culprit)
{
    m_body["culprit"] = new JSONValue<std::string>(culprit);
    return *this;
}

SentryEvent& SentryEvent::setRelease(const std::string& release)
{
    m_body["release"] = new JSONValue<std::string>(release);
    return *this;
}

