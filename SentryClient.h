//
// Created by Simon Mignot on 30/11/17.
//

#ifndef PINGCOUNT_SENTRYCLIENT_H
#define PINGCOUNT_SENTRYCLIENT_H

#include <typeinfo>
#include <restclient-cpp/connection.h>

#include "JSONValue.h"
#include "DSN.h"
#include "SentryEvent.h"

class SentryClient
{
    public:
        static bool configureClient(std::string dsn_str);
        static SentryClient* getInstance();

        static JSONObject* getSdkInfos();
        static std::string getSDKUserAgent();
        static std::string getSentryVersion();

        void displayDSNInfos() const;
        void sendEvent(SentryEvent& event);

    private:
        // Methods
        SentryClient(DSN* dsn);

        static void init();


        // Attributes
        DSN* m_dsn;
        RestClient::Connection* m_connection;

        static SentryClient* m_instance;
        static JSONObject* m_sdkInfos;
        static bool m_isInitialized;
};


#endif //PINGCOUNT_SENTRYCLIENT_H
