#!/bin/bash
echo "suppression des fichiers log genere par le script"
rm count.*
rm ping.*
echo "verification de l'existance d'un serveur sur le port 9080 :"
echo 
netstat -peanut | grep 9080
echo . 
echo "verification de l'existance de la page ping: "
curl localhost:9080/ping
echo
echo
echo "verification de l'existance de la page count: "
curl localhost:9080/count
echo 
echo
echo "---------------------------------------------------------------------------------"
echo "Utilisation de WGET !"
echo "Nous allons dans un premiers temps verifier le nombre de ping deja envoye :"
wget localhost:9080/count -q 
resultat1=$(cat count.1)
echo 
echo "nous avons actuellement " $resultat1 "ping envoye "
echo
echo "envoie d'un ping pour verifier la reception d'un pong mais aussi l'incrementation de count "
wget localhost:9080/ping -q
resultat2=$(cat ping.1)
echo $resultat2
wget localhost:9080/count -q 
resultat3=$(cat count.2)
echo "nous avons actuellement " $resultat3 "ping envoye a la page"
echo "---------------------------------------------------------------------------------"
echo
echo
echo "---------------------------------------------------------------------------------"
echo "utilisation de curl"
echo "affichage du nombre de ping envoye"
curl localhost:9080/count
echo
echo " envoie d'un ping "
curl localhost:9080/ping
echo
echo "nombre de ping envoye +1"
curl localhost:9080/count
echo
echo "---------------------------------------------------------------------------------"
echo
echo
echo "---------------------------------------------------------------------------------"
echo "utilisation de JQ "
echo
echo " affichage du nombre de ping"
curl localhost:9080/count | jq -R
echo
echo
echo "envoie d'un ping avec resultat"
curl localhost:9080/ping | jq -R
curl localhost:9080/count | jq -R
echo "---------------------------------------------------------------------------------"










