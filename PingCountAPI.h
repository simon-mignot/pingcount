//
// Created by Simon Mignot on 04/12/17.
//

#ifndef PINGCOUNT_PINGCOUNTAPI_H
#define PINGCOUNT_PINGCOUNTAPI_H

#include <pistache/http.h>
#include <pistache/router.h>

#include "JSON.h"
#include "JSONValue.h"

class PingCountAPI
{
    public:
        static void setupRoutes(Pistache::Rest::Router& router);

        static void ping(const Pistache::Rest::Request& request, Pistache::Http::ResponseWriter response);
        static void count(const Pistache::Rest::Request& request, Pistache::Http::ResponseWriter response);

    private:
        static int m_pingCount;
};


#endif //PINGCOUNT_PINGCOUNTAPI_H
