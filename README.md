# PingCount

Dépot original : https://bitbucket.org/simon-mignot/pingcount

Bibliothèques utilisées : 

* [[github] Pistache](https://github.com/oktal/pistache)
* [[github] restclient-cpp](https://github.com/mrtazz/restclient-cpp)

Installation
```
# Installation des dépendances puis de PingCount dans ./pingcount/PingCount
wget https://bitbucket.org/simon-mignot/pingcount/raw/master/install.sh
./install.sh

# Configuration du dsn
echo {private-dsn} > ./pingcount/private.dsn
#OU
#echo {public-dsn} > ./pingcount/public.dsn

# Lancement du serveur
cd ./pingcount
./PingCount
```